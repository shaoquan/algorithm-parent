package com.redtide;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 飞机售票算法实现
 * 详细需求如: docs/飞机售票算法编程.pdf
 * Created by zsq on 2022-01-11.
 */
public class SellTicket{

    public static void main(String[] args){
        int row = 0,col = 0;
//        int[][] arr = {{10,10}};
        int[][] arr = {{3,4},{4,5},{2,3},{1,11},{3,4}};
//        int[][] arr = {{3,2},{4,3},{2,3},{3,4}};
        Map<String,Position> map = new HashMap<>();
        for(int i=0;i<arr.length;i++){
            row = arr[i][1] > row ? arr[i][1] : row;
            for(int j=0;j<arr[i][1];j++){
                for(int k=0;k<arr[i][0];k++){
                    int w;
                    if(i == 0 || i==arr.length-1){
                        if((i == 0 && k == 0) || (i==arr.length-1 && k==arr[i][0]-1)){
                            w = 2;
                        }else if((i == 0 && k==arr[i][0]-1) || (i==arr.length-1 && k == 0)){
                            w = 1;
                        }else{
                            w = 3;
                        }
                    }else{
                        if(k == 0 || k==arr[i][0]-1){
                            w = 1;
                        }else{
                            w = 3;
                        }
                    }
                    map.put(j+"-"+(k+col),new Position(j,(k+col),w));
                    System.out.print(j+"-"+w+"-"+(k+col)+" ");
                }
            }
            col += arr[i][0];
            System.out.println();
        }
        System.out.println("------------------------------------------------------------------------------------------");
        List<Position> dataList = map.entrySet().stream()
                .map(Map.Entry::getValue)
                .sorted(Comparator.comparing(Position::getW).thenComparing(Position::getX).thenComparing(Position::getY))
                .collect(Collectors.toList());
        for(int i=0;i<30;i++){
            if(i < dataList.size()){
                dataList.get(i).setV(i+1);
            }
        }
        for(int i=0;i<row;i++){
            for(int j=0;j<col;j++){
                Position position = map.get(i + "-" + j);
                if(position != null){
                    System.out.print("|");
                    System.out.format("\33[%d;2m"+String.format("%02d",position.getV()),(position.getW()==1 ? 34 : (position.getW() == 2 ? 37 : 31)));
                    System.out.format("\33[%d;0m| ",30);
                }else{
                    System.out.print("     ");
                }
            }
            System.out.println();
        }
        System.out.println("------------------------------------------------------------------------------------------");
    }

    static class Position{
        private int x;
        private int y;
        private int w;
        private int v;

        public Position(int x,int y,int w){
            this.x = x;
            this.y = y;
            this.w = w;
        }

        public int getX(){
            return x;
        }

        public int getY(){
            return y;
        }

        public int getW(){
            return w;
        }

        public int getV(){
            return v;
        }

        public Position setV(int v){
            this.v = v;
            return this;
        }
    }
}